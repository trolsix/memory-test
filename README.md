--------------
Test memory. UART communication.

--------------
Test memory, command:
SAxxx - set adres xxx00
DSxxx - set xxx data
DGxxx - get xxx data
DT - test memory

low acces:
DWx - write 
DR - read
K - set clk
k - clr clk
C - set clr count
c - clr clr count
W - set write
w - set read
B - set size block or with acces
b - clr size block or with acces
R - set REG line
r - clr  REG line
A - set adres line
a - clr  adres line
8 - set interface 8 bit
4 - set interface 4 bit
h - set hihg impedance
l - set low ipmedance
z - set zero
--------------