/*-----------------------------------------------
author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*----------------------------------------------------------------------------/

/----------------------------------------------------------------------------*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdint.h>


//#define BAUD (9600UL * 8)
#define BAUD (19200UL * 8)

#if defined(NULL)
#else
#define NULL ((void*)(0))
#endif

/*--------------------------------- 

--------------------------------- */

static const uint32_t dectbl[9] = {  1,
                         10,       100,
                       1000,     10000,
                     100000,   1000000,
                   10000000, 100000000 };

void binbcd1 ( char * tbl, uint32_t ul ) {
	uint32_t in1, in2;
	unsigned char wsz, a;

	tbl[10] = 0;
	tbl[9] = 0;	

	while ( ul > 999999999 ) {
		++tbl[9];
		ul -= 1000000000;
	}
	tbl[9] += '0';	
	in1 = ul;
	for( wsz=8; wsz&0xFF; --wsz ) {
		in2 = dectbl[wsz];
		a = 0;
		while (1) {
			in1 -= in2;
			if ( in1 & 0x80000000 )
				break;
			++a;
		}
		in1 += in2;
		tbl[wsz] = a+'0';
	}
	
	tbl[0] = in1+'0';
}

/*--------------------------------- 

--------------------------------- */

void RSout( uint8_t d ) {
	while ( 0 == ( UCSR0A & (1<<UDRE0) ) );
	UDR0 = d;
}

static void RSouttbl ( unsigned char siz, const unsigned char *tblf ) {
	if ( !siz ) {
		while(1) {
			RSout ( *tblf );
			++tblf;
			if ( *tblf == 0 ) break;
		}
		return;
	}
	while(siz) RSout ( tblf[--siz] );
}

/*--------------------------------- 

--------------------------------- */

#define CLRCBIT 3
#define WRITBIT 2
#define SBLKBIT 1
#define IN48BIT 1
#define ADRTBIT 0

/*--------------------------------- 

dat PB 4-7

--------------------------------- */

void setdata4 ( uint8_t d ) {
	//PORTD |= (PORTD&0x0F) | ((d<<4)&0x0F);
	PORTB = (PORTB&0xF0) | (d&0x0F);
}


void setw ( void ) { PORTB |= 1<<WRITBIT; }
void setr ( void ) { PORTB &= ~(1<<WRITBIT); }

void setclrc ( void ) {	PORTB |= 1<<CLRCBIT; }
void clrclrc ( void ) {	PORTB &= ~(1<<CLRCBIT); }

void setblk ( void ) { PORTB |= 1<<SBLKBIT; }
void clrblk ( void ) { PORTB &= ~(1<<SBLKBIT); }

void setadr ( void ) { PORTB |= 1<<ADRTBIT; }
void clradr ( void ) { PORTB &= ~(1<<ADRTBIT); }

void clkset ( void ) { PORTD |= 1<<6; }
void clkclr ( void ) { PORTD &= ~(1<<6); }

void regset ( void ) { PORTD |= 1<<7; }
void regclr ( void ) { PORTD &= ~(1<<7); }

void plim ( void ) { DDRB = 0xFF; DDRC = 0xFF; }
void phim ( void ) { DDRB = 0xF0; DDRC = 0xF0; }

void writadr ( uint8_t d ) {
	uint8_t adr;
	
	adr = 1<<ADRTBIT;
	if(d&0x01) adr |= 0x08;
	if(d&0x02) adr |= 0x04;
	if(d&0x04) adr |= 0x02;
	
	PORTB = (PORTB&0xF0) | adr;
	
	clkset();
	clkclr();
}

/*--------------------------------- 

--------------------------------- */

void uart_init ( void ) {
	UBRR0L = (F_CPU / BAUD) - 1;
	UBRR0H = ( (F_CPU / BAUD) - 1 ) >> 8;
	
	UCSR0A = 1<<U2X0;
	UCSR0C = (3<<UCSZ00);
	UCSR0B = (1<<TXEN0) | (1<<RXEN0) | (1<<RXCIE0);
	//UDR;
}

void init() {
	PORTC = 0;
	PORTD = 0;
	PORTB = 0;
	DDRD = 0xF0;
	DDRB = 0xFF;
	DDRC = 0x10;

}

/*--------------------------------- 

--------------------------------- */

char buf2[16];
uint8_t buf[32];
volatile uint8_t getdata;
volatile uint8_t flagram;
volatile uint8_t blkadr;

/*--------------------------------- 

--------------------------------- */

int main (void) {

	init();	
	uart_init ();

	getdata = 0;
	flagram = 0;
	blkadr = 0;
	
	sei();

	RSout('S');
	RSout('T');
	RSout('A');
	RSout('R');
	RSout('T');
	RSout('\n');
	
	while(1) {	

	}

	
	return 0;
}

/*--------------------------------- 
easy test ;)
--------------------------------- */

int16_t getnumb ( char * d ) {
	int16_t numb = 0;
	while(1){
		if ( d[0] < '0' ) break;
		numb = d[0] - '0';
		if ( d[1] < '0' ) break;
		numb = (numb*10) + (d[1] - '0');
		if ( d[2] < '0' ) break;
		numb = (numb*10) + (d[2] - '0');
		break;
	}
	return numb;
}

/*--------------------------------- 

--------------------------------- */

uint8_t ramread ( void ) {
	
	uint8_t d;
	
	if(flagram&0x01) {
		clkset();
		d = PINC & 0x0F;
		d <<= 4;
		d |= PINB & 0x0F;
		clkclr();
		return d;
	}
		
	d = PINB & 0x0F;
	clkset();
	d <<= 4;
	d |= PINB & 0x0F;
	clkclr();
	return d;
}

/*--------------------------------- 

--------------------------------- */

void ramwrit ( uint8_t d ) {
	
	if(flagram&0x01) {
		PORTB = (PORTB&0xF0) | ((d)&0x0F);
		PORTC = (PORTC&0xF0) | ((d>>4)&0x0F);
		clkset();
	} else {
		setdata4 ( d >> 4 );
		clkset();
		setdata4 ( d );
	}
	_delay_us(0.2);
	clkclr();
}

/*--------------------------------- 

--------------------------------- */

void blkset ( uint8_t blkadr ) {
	
	regset ();
	plim ();
	setdata4 ( 0 );
	setw ();
	clkset();
	clkclr();
	
	writadr ( blkadr >> 6);
	writadr ( blkadr >> 3);
	writadr ( blkadr >> 0);
	
	setdata4 ( 0 );
	phim ();
	regclr ();	
}

void readset (void){
	regset ();
	plim ();
	setdata4 ( 0 );
	setclrc();
	clkset();
	clkclr();
	phim ();
	setdata4 ( 0 );
	regclr ();
}

void writeset(void) {
	regset ();
	plim ();
	setdata4 ( 0 );
	setclrc();
	setw ();
	clkset();
	clkclr();
	setdata4 ( 0 );
	regclr ();
}

/*--------------------------------- 

--------------------------------- */

ISR(USART_RX_vect) {
	
	getdata &= 0x0F;
	
	buf[getdata] = UDR0;
	
	//RSout(buf[getdata]);
	
	while ( buf[getdata] == '\n' ) {
		
		if ( buf[0] == 'T' ) {
			RSout('Y');
			RSout('\n');
		}
		
		if ( buf[0] == 'K' ) clkset ();
		else if ( buf[0] == 'k' ) clkclr ();
		else if ( buf[0] == 'C' ) setclrc ();
		else if ( buf[0] == 'c' ) clrclrc ();
		else if ( buf[0] == 'W' ) setw ();
		else if ( buf[0] == 'w' ) setr ();
		else if ( buf[0] == 'B' ) setblk ();
		else if ( buf[0] == 'b' ) clrblk ();
		else if ( buf[0] == 'R' ) regset();
		else if ( buf[0] == 'r' ) regclr();
		else if ( buf[0] == 'A' ) setadr();
		else if ( buf[0] == 'a' ) clradr();
		else if ( buf[0] == '8' ) flagram |= 0x01;
		else if ( buf[0] == '4' ) flagram &= ~0x01;
		else if ( buf[0] == 'h' ) phim (); /* high impedance */
		else if ( buf[0] == 'l' ) plim (); /* low impedance */
		else if ( buf[0] == 'z' ) setdata4 (0); /* port zero */
		
		/* set adr */
		if ( ( buf[0] == 'S' ) && ( buf[1] == 'A' ) ) {
			blkadr = 0x7F & getnumb ( (void*)&buf[2] );
			blkset(blkadr);
		}
		
		
		if ( ( buf[0] == 'D' ) && ( buf[1] == 'W' ) ) {
			ramwrit(buf[2]);
		}
		
		
		if ( ( buf[0] == 'D' ) && ( buf[1] == 'R' ) ) {
			RSout('_');
			RSout( ramread() );
			RSout('_');
			RSout('\n');
		}
		
		
		/* save xxx data a-z */
		if ( ( buf[0] == 'D' ) && ( buf[1] == 'S' ) ) {
			
			uint16_t siz;
			char d, n[3];
			
			siz = getnumb ( (void*)&buf[2] );
			
			//set write size block and clr countblock
			writeset();
			
			/* data write for test write 4 */
			d = blkadr;
			siz >>= 2;
			
			if(d>100) { n[2] = '1'; d -= 100; }
			else n[2] = '0';
			n[1] = (d / 10)+'0';
			n[0] = (d % 10)+'0';
			
			for( d='a'; siz; --siz ) {
				ramwrit(n[2]);
				ramwrit(n[1]);
				ramwrit(n[0]);
				ramwrit(d);
				if ( ++d > 'z' ) d = 'a';
			}
		}
		
		
		/* read 512 data send */
		if ( ( buf[0] == 'D' ) && ( buf[1] == 'G' ) ) {
			
			uint16_t siz;
			char d;
			
			siz = getnumb ( (void*)&buf[2] );
			
			//set read size block and clr countblock
			readset();
		
			d = 'a';
			
			_delay_us(0.2);
			
			/* data read for test x4 */
			//siz >>= 2;
			
			for(;siz;--siz) {
				d = ramread();
				RSout(d);
			}
			RSout('\n');
			RSout('\n');
		}
		
		
		/* memory test */
		/* test pattern for every block 512 bytes */
		if ( ( buf[0] == 'M' ) && ( buf[1] == 'T' ) ) {
			uint8_t d, block;
			uint16_t siz;
			
			/* add 2 because adr block is with 256 step */
			for ( block=0; block<128; block+=2 ) {
				blkset(block);
				
				/* set one */
				for ( d=0x01; d; d<<=1 ) {
					writeset();
					for(siz=512;siz;--siz) {
						ramwrit(d);
					}
					readset();
					for(siz=512;siz;--siz) {
						if( d != ramread() ) {
							RSouttbl ( 0, (void*)"Bad block " );
							binbcd1 ( (void*)buf2, block );
							RSouttbl ( 6, (void*)buf2 );
							RSouttbl ( 0, (void*)" T1\n" );
							getdata = 0;
							return;
						}
					}
				}
				
				
				/* clr one */
				for ( d=0x01; d; d<<=1 ) {
					writeset();
					for(siz=512;siz;--siz) {
						ramwrit(~d);
					}
					readset();
					d = ~d;
					for(siz=512;siz;--siz) {
						//if( (~d) != ramread() ) {
						if ( d != ramread() ) {
							RSouttbl ( 0, (void*)"Bad block " );
							binbcd1 ( buf2, block );
							RSouttbl ( 6, buf2 );
							RSouttbl ( 0, (void*)" T2\n" );
							getdata = 0;
							return;
						}
					}
					d = ~d;
				}
				
				
				/* count */
				d = 0;
				writeset();
				for(siz=512;siz;--siz) {
					ramwrit(d);
					d++;
				}
				
				d = 0;
				readset();
				for(siz=512;siz;--siz) {
					if( d != ramread() ) {
						RSouttbl ( 0, (void*)"Bad block " );
						binbcd1 ( buf2, block );
						RSouttbl ( 6, buf2 );
						RSouttbl ( 0, (void*)" T3\n" );
						getdata = 0;
						return;
					}
					d++;
				}
			
				RSouttbl ( 0, (void*)"Block " );
				binbcd1 ( buf2, block );
				RSouttbl ( 6, buf2 );
				RSouttbl ( 0, (void*)" OK\n" );
			}
			
			
		}
		
		
		getdata = 0;
		return;
	}

	
	getdata++;
}


ISR(TIMER0_OVF_vect) {

}


ISR(TIMER1_OVF_vect) {

}

/*--------------------------------- 

--------------------------------- */
